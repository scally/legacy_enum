module LegacyEnum
  class MethodDefinitionDSL
    def initialize params
      @enum_def = []
      @legacy_column_name = params.fetch :legacy_column_name
    end

    def method_missing(symbol, *args)
      options = args.extract_options!

      options.merge! name: symbol
      options.merge! value: args[0] unless args.empty?
      options.merge! label: symbol.to_s.titleize unless options.keys.include?(:label)

      @enum_def << options
    end

    def legacy_column_name
      @legacy_column_name
    end

    def named name
      find_config key: :name, value: name
    end

    def valued value
      find_config key: :value, value: value
    end

    def each &block
      @enum_def.each { |definition| block.call definition }
    end

    protected

    def null_definition
      { name: nil, value: nil, label: nil }
    end

    def find_config params
      key = params.fetch :key
      value = params.fetch :value

      @enum_def.find { |config| config[key].to_s.casecmp(value.to_s) == 0 } || null_definition
    end
  end
end