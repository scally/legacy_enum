module LegacyEnum
  module ClassMethods
    def legacy_enum(name, *options, &block)
      extracted_options = options.extract_options!
      legacy_column_name = extracted_options[:lookup].try(:to_s) || "#{name.to_s.capitalize}ID"

      config = MethodDefinitionDSL.new legacy_column_name: legacy_column_name
      config.instance_eval(&block)

      cattr_accessor :enum_configs unless defined? self.enum_configs
      self.enum_configs ||= {}

      self.enum_configs[name] = config

      class_eval do

        define_method "#{name}_changed?" do
          send "#{legacy_column_name}_changed?"
        end

        define_method name do
          enum_configs[name].valued(legacy_value(name))[:name]
        end

        define_method "#{name}=" do |value|
          normalized_value = value.nil? ? value : value.to_sym
          set_value = enum_configs[name].named(normalized_value)[:value]
          set_legacy_value name, set_value
        end

        define_method "#{name}_label" do
          enum_configs[name].valued(legacy_value(name))[:label]
        end

        def legacy_value(name)
          send enum_configs[name].legacy_column_name.to_sym
        end

        def set_legacy_value(name, value)
          send "#{enum_configs[name].legacy_column_name}=".to_sym, value
        end

        return unless extracted_options[:scope]

        scope name.to_sym,
          lambda { |enum_value| where(legacy_column_name => enum_configs[name].named(enum_value)[:value] ) }

        enum_configs[name].each do |config|
          singleton_class.instance_eval do
            if extracted_options[:scope] == :one
              define_method config[:name].to_sym, lambda { send(name, config[:name]).first }
            else
              define_method config[:name].to_sym, lambda { send(name, config[:name]) }
            end
          end
        end

      end

    end

    # Returns all enumerations for the class by enum_name and then name => value
    def enums
      filter_config config_part_name: :name, config_part_value: :value
    end

    # Returns all enumerations for the class by enum_name and then name => label
    def labels
      filter_config config_part_name: :name, config_part_value: :label
    end

    # Returns all enumerations for the class by enum_name and then value => name
    def enum_ids
      filter_config config_part_name: :value, config_part_value: :name
    end

    protected
    def filter_config(params={})
      config_part_name = params.fetch :config_part_name
      config_part_value = params.fetch :config_part_value

      enums = {}
      enum_configs.each do |name, config|
        enums[name] ||= {}
        config.each do |pair|
          enums[name][pair[config_part_name]] = pair[config_part_value]
        end
      end
      enums
    end
  end
end
